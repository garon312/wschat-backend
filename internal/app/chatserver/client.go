package chatserver

import (
	"chat-backend/internal/app/model"
	"context"
	"encoding/json"
	"errors"
	"log"
	"time"

	"github.com/gorilla/websocket"
)

const (
	// Max wait time when writing message to peer
	writeWait = 10 * time.Second

	// Max time till next pong from peer
	pongWait = 60 * time.Second

	// Send ping interval, must be less then pong wait time
	pingPeriod = (pongWait * 9) / 10

	// Maximum message size allowed from peer.
	maxMessageSize = 10000
)

var newline = []byte{'\n'}

// Client represents the websocket client at the server
type Client struct {
	ID         string `json:"id"`
	Name       string `json:"name"`
	conn       *websocket.Conn
	chatServer *ChatServer
	send       chan []byte
	rooms      map[*Room]bool
}

func (c *Client) GetID() string {
	return c.ID
}

func (c *Client) SetID(ID string) {
	c.ID = ID
}

func (c *Client) GetName() string {
	return c.Name
}

func NewClient(conn *websocket.Conn, chatServer *ChatServer, name string) *Client {
	return &Client{
		conn:       conn,
		chatServer: chatServer,
		send:       make(chan []byte, 256),
		rooms:      make(map[*Room]bool),
		Name:       name,
	}
}

func (c *Client) handleNewMessage(rawMessage []byte) {
	var message *Message

	if err := json.Unmarshal(rawMessage, &message); err != nil {
		log.Printf("Error on unmarshal JSON message %s", err)
	}

	message.Sender = c

	switch message.Action {
	case SendMessageAction:
		roomID := message.Target.GetID()

		if room := c.chatServer.findRoomByID(roomID); room != nil {
			room.broadcast <- message
		}

	case JoinRoomAction:
		if err := c.handleJoinRoomMessage(message); err != nil {
			c.chatServer.Logger.Errorln(err)
		}
	case LeaveRoomAction:
		c.handleLeaveRoomMessage(message)
	case JoinRoomPrivateAction:
		if err := c.handleJoinRoomPrivateMessage(message); err != nil {
			c.chatServer.Logger.Errorln(err)
		}
	}
}

func (c *Client) handleJoinRoomMessage(message *Message) error {
	roomName := message.Message

	if _, err := c.joinRoom(roomName, nil); err != nil {
		return err
	}

	return nil
}

func (c *Client) handleJoinRoomPrivateMessage(message *Message) error {
	target := c.chatServer.findUserByID(message.Message)

	if target == nil {
		return errors.New("target is nil")
	}

	roomName := message.Message + c.GetID()

	joinedRoom, err := c.joinRoom(roomName, target)

	if err != nil {
		return err
	}

	if joinedRoom != nil {
		if err := c.inviteTargetUser(target, joinedRoom); err != nil {
			return err
		}
	}

	return nil
}

func (c *Client) joinRoom(roomName string, sender model.User) (*Room, error) {
	room, err := c.chatServer.findRoomByName(roomName)

	if err != nil {
		return nil, err
	}

	if room == nil {
		room, err = c.chatServer.createRoom(roomName, sender != nil)

		if err != nil {
			return nil, err
		}
	}

	if sender == nil && room.IsPrivate() {
		return nil, nil
	}

	if !c.isInRoom(room) {
		c.rooms[room] = true
		room.register <- c
		c.notifyRoomJoined(room, sender)
	}

	return room, nil
}

func (c *Client) inviteTargetUser(target model.User, room *Room) error {
	inviteMessage := &Message{
		Action:  JoinRoomPrivateAction,
		Message: target.GetID(),
		Target:  room,
		Sender:  c,
	}

	return c.chatServer.Redis.Publish(context.Background(), PubSubGeneralChannel, inviteMessage.encode()).Err()
}

func (c *Client) isInRoom(room *Room) bool {
	if _, ok := c.rooms[room]; ok {
		return true
	}

	return false
}

func (c *Client) notifyRoomJoined(room *Room, sender model.User) {
	message := Message{
		Action: RoomJoinedAction,
		Target: room,
		Sender: sender,
	}

	c.send <- message.encode()
}

func (c *Client) handleLeaveRoomMessage(message *Message) {
	room := c.chatServer.findRoomByID(message.Message)

	if room == nil {
		return
	}

	if _, ok := c.rooms[room]; ok {
		delete(c.rooms, room)
	}

	room.unregister <- c
}

func (c *Client) ReadPump() {
	defer func() {
		c.disconnect()
	}()

	c.conn.SetReadLimit(maxMessageSize)

	if err := c.conn.SetReadDeadline(time.Now().Add(pongWait)); err != nil {
		return
	}

	c.conn.SetPongHandler(func(string) error {
		if err := c.conn.SetReadDeadline(time.Now().Add(pongWait)); err != nil {
			return err
		}

		return nil
	})

	for {
		_, jsonMessage, err := c.conn.ReadMessage()

		if err != nil {
			break
		}

		c.handleNewMessage(jsonMessage)
	}
}

func (c *Client) WritePump() {
	ticker := time.NewTicker(pingPeriod)
	defer func() {
		ticker.Stop()
		if err := c.conn.Close(); err != nil {
			return
		}
	}()

	for {
		select {
		case message, ok := <-c.send:
			if err := c.conn.SetWriteDeadline(time.Now().Add(writeWait)); err != nil {
				return
			}

			if !ok {
				// The WsServer closed the channel.
				_ = c.conn.WriteMessage(websocket.CloseMessage, []byte{})
				return
			}

			w, err := c.conn.NextWriter(websocket.TextMessage)

			if err != nil {
				return
			}

			if _, err = w.Write(message); err != nil {
				return
			}

			// Attach queued chat messages to the current websocket message.
			n := len(c.send)

			for i := 0; i < n; i++ {
				if _, err = w.Write(newline); err != nil {
					return
				}

				if _, err = w.Write(<-c.send); err != nil {
					return
				}
			}

			if err := w.Close(); err != nil {
				return
			}
		case <-ticker.C:
			if err := c.conn.SetWriteDeadline(time.Now().Add(writeWait)); err != nil {
				return
			}

			if err := c.conn.WriteMessage(websocket.PingMessage, nil); err != nil {
				return
			}
		}
	}
}

func (c *Client) disconnect() {
	c.chatServer.Unregister <- c
	for room := range c.rooms {
		room.unregister <- c
	}

	close(c.send)
	_ = c.conn.Close()
}
