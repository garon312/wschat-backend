package chatserver

import (
	"chat-backend/internal/app/model"
	"chat-backend/internal/app/store"
	"context"
	"encoding/json"

	"github.com/go-redis/redis/v8"
	"github.com/sirupsen/logrus"
)

const PubSubGeneralChannel = "general"

type ChatServer struct {
	Clients    map[*Client]bool
	Register   chan *Client
	Unregister chan *Client
	Broadcast  chan []byte
	Rooms      map[*Room]bool
	Users      []model.User
	Logger     *logrus.Logger
	Store      store.Store
	Redis      *redis.Client
}

func NewChatServer(logger *logrus.Logger, store store.Store, redis *redis.Client) (*ChatServer, error) {
	server := &ChatServer{
		Clients:    make(map[*Client]bool),
		Register:   make(chan *Client),
		Unregister: make(chan *Client),
		Rooms:      make(map[*Room]bool),
		Logger:     logger,
		Store:      store,
		Redis:      redis,
	}

	users, err := store.User().GetAllUsers()

	if err != nil {
		return nil, err
	}

	server.Users = users

	return server, nil
}

func (s *ChatServer) Run() {
	go s.listenPubSubChannel()

	for {
		select {
		case client := <-s.Register:
			s.Logger.Print("Client connected to the channel")
			if err := s.registerClient(client); err != nil {
				s.Logger.Error(err)
			}
		case client := <-s.Unregister:
			s.Logger.Print("Client disconnected from the channel")
			if err := s.unregisterClient(client); err != nil {
				s.Logger.Error(err)
			}
		case message := <-s.Broadcast:
			s.Logger.Print(string(message))
			s.broadcastToClients(message)
		}
	}
}

func (s *ChatServer) registerClient(client *Client) error {
	if err := s.Store.User().AddUser(client); err != nil {
		return err
	}

	if err := s.publishClientJoined(client); err != nil {
		return err
	}

	s.listOnlineClients(client)
	s.Clients[client] = true

	return nil
}

func (s *ChatServer) unregisterClient(client *Client) error {
	if _, ok := s.Clients[client]; ok {
		delete(s.Clients, client)
		if err := s.Store.User().RemoveUser(client); err != nil {
			return err
		}

		if err := s.publishClientLeft(client); err != nil {
			return err
		}
	}

	return nil
}

func (s *ChatServer) broadcastToClients(message []byte) {
	for client := range s.Clients {
		client.send <- message
	}
}

func (s *ChatServer) findRoomByID(ID string) *Room {
	var foundRoom *Room

	for room := range s.Rooms {
		if room.GetID() == ID {
			foundRoom = room
			break
		}
	}

	return foundRoom
}

func (s *ChatServer) findClientByID(ID string) *Client {
	var foundClient *Client

	for client := range s.Clients {
		if client.ID == ID {
			foundClient = client
			break
		}
	}

	return foundClient
}

func (s *ChatServer) findRoomByName(name string) (*Room, error) {
	var foundRoom *Room
	var err error

	for room := range s.Rooms {
		if room.GetName() == name {
			foundRoom = room
			break
		}
	}

	if foundRoom == nil {
		foundRoom, err = s.runRoomFromRepository(name)

		if err != nil {
			return nil, err
		}
	}

	return foundRoom, err
}

func (s *ChatServer) runRoomFromRepository(name string) (*Room, error) {
	var room *Room

	dbRoom, err := s.Store.Room().FindRoomByName(name)

	if err != store.ErrNotFound {
		return nil, err
	}

	if dbRoom != nil {
		room = NewRoom(s, dbRoom.GetName(), dbRoom.IsPrivate())

		go room.RunRoom()
		s.Rooms[room] = true
	}

	return room, nil
}

func (s *ChatServer) createRoom(name string, private bool) (*Room, error) {
	room := NewRoom(s, name, private)

	err := s.Store.Room().AddRoom(room)

	if err != nil {
		return nil, err
	}

	go room.RunRoom()
	s.Rooms[room] = true

	return room, nil
}

func (s *ChatServer) listOnlineClients(client *Client) {
	for _, user := range s.Users {
		message := &Message{
			Action: UserJoinedAction,
			Sender: user,
		}

		client.send <- message.encode()
	}
}

func (s *ChatServer) publishClientJoined(client *Client) error {
	message := &Message{
		Action: UserJoinedAction,
		Sender: client,
	}

	if err := s.Redis.Publish(context.Background(), PubSubGeneralChannel, message.encode()).Err(); err != nil {
		s.Logger.Errorln(err)
	}

	return nil
}

func (s *ChatServer) publishClientLeft(client *Client) error {

	message := &Message{
		Action: UserLeftAction,
		Sender: client,
	}

	if err := s.Redis.Publish(context.Background(), PubSubGeneralChannel, message.encode()).Err(); err != nil {
		return err
	}

	return nil
}

func (s *ChatServer) listenPubSubChannel() {

	pubSub := s.Redis.Subscribe(context.Background(), PubSubGeneralChannel)
	ch := pubSub.Channel()
	for msg := range ch {

		var message Message
		if err := json.Unmarshal([]byte(msg.Payload), &message); err != nil {
			s.Logger.Errorf("Error on unmarshal JSON message %s", err)
			return
		}

		switch message.Action {
		case UserJoinedAction:
			s.handleUserJoined(message)
		case UserLeftAction:
			s.handleUserLeft(message)
		case JoinRoomPrivateAction:
			if err := s.handleUserJoinPrivate(message); err != nil {
				s.Logger.Errorln(err)
			}
		}
	}
}

func (s *ChatServer) handleUserJoined(message Message) {
	s.Users = append(s.Users, message.Sender)
	s.broadcastToClients(message.encode())
}

func (s *ChatServer) handleUserLeft(message Message) {
	for i, user := range s.Users {
		if user.GetID() == message.Sender.GetID() {
			s.Users[i] = s.Users[len(s.Users)-1]
			s.Users = s.Users[:len(s.Users)-1]
		}
	}

	s.broadcastToClients(message.encode())
}

func (s *ChatServer) handleUserJoinPrivate(message Message) error {
	targetClient := s.findClientByID(message.Message)
	if targetClient != nil {
		_, err := targetClient.joinRoom(message.Target.GetName(), message.Sender)

		if err != nil {
			return err
		}
	}

	return nil
}

func (s *ChatServer) findUserByID(ID string) model.User {
	var foundUser model.User

	for _, client := range s.Users {
		if client.GetID() == ID {
			foundUser = client
			break
		}
	}

	return foundUser
}
