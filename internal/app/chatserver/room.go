package chatserver

import (
	"context"
	"fmt"
)

const (
	welcomeMessage = "Welcome, %s!"
	leftMessage = "%s has left"
)

type Room struct {
	ID         string `json:"id"`
	Name       string `json:"name"`
	Private    bool   `json:"private"`
	chatServer *ChatServer
	clients    map[*Client]bool
	register   chan *Client
	unregister chan *Client
	broadcast  chan *Message
}

// NewRoom creates a new Room
func NewRoom(chatServer *ChatServer, name string, private bool) *Room {
	return &Room{
		Name:       name,
		Private:    private,
		chatServer: chatServer,
		clients:    make(map[*Client]bool),
		register:   make(chan *Client),
		unregister: make(chan *Client),
		broadcast:  make(chan *Message),
	}
}

// GetID returns the id of the room
func (r *Room) GetID() string {
	return r.ID
}

// GetID returns the id of the room
func (r *Room) SetID(ID string) {
	r.ID = ID
}

// GetName returns the name of the room
func (r *Room) GetName() string {
	return r.Name
}

// IsPrivate checks if the room is private
func (r *Room) IsPrivate() bool {
	return r.Private
}

// RunRoom runs our room, accepting various requests
func (r *Room) RunRoom() {
	go r.subscribeToRoomMessages()

	for {
		select {
		case client := <-r.register:
			if err := r.registerClientInRoom(client); err != nil {
				r.chatServer.Logger.Errorln(err)
			}

		case client := <-r.unregister:
			if err := r.unregisterClientInRoom(client); err != nil {
				r.chatServer.Logger.Errorln(err)
			}

		case message := <-r.broadcast:
			if err := r.publishRoomMessage(message.encode()); err != nil {
				r.chatServer.Logger.Errorln(err)
			}
		}
	}
}

func (r *Room) registerClientInRoom(client *Client) error {
	if !r.IsPrivate() {
		if err := r.notifyClientJoined(client); err != nil {
			return err
		}
	}

	r.clients[client] = true

	return nil
}

func (r *Room) unregisterClientInRoom(client *Client) error {
	if _, ok := r.clients[client]; ok {
		if err := r.notifyClientLeft(client); err != nil {
			return err
		}

		delete(r.clients, client)
	}

	return nil
}

func (r *Room) broadcastToClientsInRoom(message []byte) {
	for client := range r.clients {
		// TODO: send on closed channel when client disconnected
		client.send <- message
	}
}

func (r *Room) notifyClientJoined(client *Client) error {
	message := &Message{
		Action:  SendMessageAction,
		Message: fmt.Sprintf(welcomeMessage, client.Name),
		Target:  r,
	}

	if err := r.publishRoomMessage(message.encode()); err != nil {
		return err
	}

	return nil
}

func (r *Room) notifyClientLeft(client *Client) error {
	message := &Message{
		Action:  SendMessageAction,
		Message: fmt.Sprintf(leftMessage, client.Name),
		Target:  r,
	}

	if err := r.publishRoomMessage(message.encode()); err != nil {
		return err
	}

	return nil
}

func (r *Room) publishRoomMessage(message []byte) error {
	err := r.chatServer.Redis.Publish(context.Background(), r.GetName(), message).Err()

	if err != nil {
		return err
	}

	return nil
}

func (r *Room) subscribeToRoomMessages() {
	pubSub := r.chatServer.Redis.Subscribe(context.Background(), r.GetName())

	ch := pubSub.Channel()

	for msg := range ch {
		r.broadcastToClientsInRoom([]byte(msg.Payload))
	}
}