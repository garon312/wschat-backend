package chatserver

import (
	"chat-backend/internal/app/model"
	"encoding/json"
	"log"
)

const (
	SendMessageAction     = "send-message"
	JoinRoomAction        = "join-room"
	LeaveRoomAction       = "leave-room"
	UserJoinedAction      = "user-join"
	UserLeftAction        = "user-left"
	JoinRoomPrivateAction = "join-room-private"
	RoomJoinedAction      = "room-joined"
)

type Message struct {
	Action  string      `json:"action"`
	Message string      `json:"message"`
	Target  *Room       `json:"target"`
	Sender  model.User `json:"sender"`
}

func (m *Message) encode() []byte {
	result, err := json.Marshal(m)

	if err != nil {
		log.Println(err)
	}

	return result
}

// UnmarshalJSON custom unmarshal to create a Client instance for Sender
func (m *Message) UnmarshalJSON(data []byte) error {
	type Alias Message
	msg := &struct {
		Sender Client `json:"sender"`
		*Alias
	}{
		Alias: (*Alias)(m),
	}

	if err := json.Unmarshal(data, &msg); err != nil {
		return err
	}
	m.Sender = &msg.Sender

	return nil
}
