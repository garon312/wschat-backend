package httpserver

// Config provides basic config structure for server
type Config struct {
	BindAddr     string `toml:"bind_addr"`
	LogLevel     string `toml:"log_level"`
	DatabaseType string `toml:"database_type"`
	DatabaseURL  string `toml:"database_url"`
	RedisURL     string `toml:"redis_url"`
}

// NewConfig instantiates default config
func NewConfig() *Config {
	return &Config{
		BindAddr: ":8080",
		LogLevel: "debug",
	}
}
