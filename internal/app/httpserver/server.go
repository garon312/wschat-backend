package httpserver

import (
	"chat-backend/internal/app/chatserver"
	"log"
	"net/http"

	"github.com/gorilla/mux"
	"github.com/gorilla/websocket"
	"github.com/sirupsen/logrus"
)

type server struct {
	router     *mux.Router
	logger     *logrus.Logger
	chatServer *chatserver.ChatServer
}

var upgrader = websocket.Upgrader{
	ReadBufferSize:  4096,
	WriteBufferSize: 4096,
	CheckOrigin: func(r *http.Request) bool {
		return true
	},
}

func newServer(logger *logrus.Logger, chatServer *chatserver.ChatServer) *server {
	s := &server{
		router:     mux.NewRouter(),
		logger:     logger,
		chatServer: chatServer,
	}

	s.configureRouter()

	return s
}

func (s *server) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	s.router.ServeHTTP(w, r)
}

func (s *server) configureRouter() {
	s.router.HandleFunc("/ws", s.handleWebsocket())
}

func (s *server) handleWebsocket() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		conn, err := upgrader.Upgrade(w, r, nil)
		if err != nil {
			log.Println(err)
			return
		}

		name := r.URL.Query().Get("name")

		if name == "" {
			return
		}

		client := chatserver.NewClient(conn, s.chatServer, name)

		go client.ReadPump()
		go client.WritePump()

		s.chatServer.Register <- client
	}
}
