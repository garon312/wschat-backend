package httpserver

import (
	"chat-backend/internal/app/chatserver"
	"chat-backend/internal/app/store"
	"chat-backend/internal/app/store/mongostore"
	"errors"
	"net/http"

	"github.com/go-redis/redis/v8"
	"github.com/sirupsen/logrus"
)

func Start(config *Config) error {
	logger := newLogger(config.LogLevel)

	db, err := newStore(config.DatabaseType, config.DatabaseURL)

	if err != nil {
		return err
	}

	if err := db.Connect(); err != nil {
		return err
	}

	redis, err := newRedisClient(config.RedisURL)

	if err != nil {
		return err
	}

	chatServer, err := chatserver.NewChatServer(logger, db, redis)

	if err != nil {
		return err
	}

	s := newServer(logger, chatServer)

	logger.Printf("Starting server on %s", config.BindAddr)

	go chatServer.Run()

	if err := http.ListenAndServe(config.BindAddr, s); err != nil {
		return err
	}

	return db.Disconnect()
}

func newLogger(logLevel string) *logrus.Logger {
	var level logrus.Level

	switch logLevel {
	case "info":
		level = logrus.InfoLevel
	case "warn":
		level = logrus.WarnLevel
	case "error":
		level = logrus.ErrorLevel
	case "fatal":
		level = logrus.FatalLevel
	default:
		level = logrus.DebugLevel
	}

	logger := logrus.New()
	logger.SetLevel(level)

	return logger
}

func newStore(dbType, dbURL string) (store.Store, error) {
	var st store.Store
	var err error

	switch dbType {
	case "mongodb":
		st, err = mongostore.New(dbURL)
	default:
		err = errors.New("unsupported database")
	}

	return st, err
}

func newRedisClient(redisURL string) (*redis.Client, error) {
	opt, err := redis.ParseURL(redisURL)
	if err != nil {
		return nil, err
	}

	return redis.NewClient(opt), nil
}