package store

// Store represents main store interface containing different repositories
type Store interface {
	Room() RoomRepository
	User() UserRepository
	Connect() error
	Disconnect() error
}
