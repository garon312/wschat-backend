package mongostore_test

import (
	"os"
	"testing"
)

var databaseURL string

func TestMain(m *testing.M) {
	databaseURL = os.Getenv("DATABASE_URL")

	if databaseURL == "" {
		databaseURL = "mongodb://mongoadmin:secret@localhost:27017"
	}

	os.Exit(m.Run())
}
