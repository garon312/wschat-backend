package mongostore

import (
	"chat-backend/internal/app/model"
	"chat-backend/internal/app/store"
	"context"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

type Room struct {
	ID      string `bson:"_id,omitempty"`
	Name    string `bson:"name,omitempty"`
	Private bool   `bson:"private,omitempty"`
}

func (room *Room) GetID() string {
	return room.ID
}

func (room *Room) SetID(ID string) {
	room.ID = ID
}

func (room *Room) GetName() string {
	return room.Name
}

func (room *Room) IsPrivate() bool {
	return room.Private
}

type RoomRepository struct {
	store *Store
}

func (r RoomRepository) AddRoom(room model.Room) error {
	res, err := r.store.client.
		Database("chatserver").
		Collection("rooms").
		InsertOne(context.Background(), room)

	if err != nil {
		return err
	}

	room.SetID((res.InsertedID).(primitive.ObjectID).Hex())

	return nil
}

func (r RoomRepository) FindRoomByName(name string) (model.Room, error) {
	filter := bson.M{"name": name}
	result := &Room{}

	if err := r.store.client.
		Database("chatserver").
		Collection("rooms").
		FindOne(context.Background(), filter).
		Decode(&result); err != nil {
			if err == mongo.ErrNoDocuments {
				return nil, store.ErrNotFound
			}

			return nil, err
	}

	return result, nil
}
