package mongostore

import (
	"chat-backend/internal/app/model"
	"chat-backend/internal/app/store"
	"context"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

type User struct {
	ID      string `bson:"_id,omitempty"`
	Name    string `bson:"name,omitempty"`
}

func (u *User) GetID() string {
	return u.ID
}

func (u *User) GetName() string {
	return u.Name
}

func (u *User) SetID(ID string) {
	u.ID = ID
}

type UserRepository struct {
	store *Store
}

func (u UserRepository) AddUser(user model.User) error {
	res, err := u.store.client.
		Database("chatserver").
		Collection("users").
		InsertOne(context.Background(), user)

	if err != nil {
		return err
	}

	user.SetID((res.InsertedID).(primitive.ObjectID).Hex())

	return nil
}

func (u UserRepository) RemoveUser(user model.User) error {
	mongoID, err := primitive.ObjectIDFromHex(user.GetID())

	if err != nil {
		return err
	}

	filter := bson.M{"_id": mongoID}

	res, err := u.store.client.
		Database("chatserver").
		Collection("users").
		DeleteOne(context.Background(), filter)

	if err != nil {
		return err
	}

	if res.DeletedCount == 0 {
		return store.ErrNotFound
	}

	return nil
}

func (u UserRepository) FindUserByID(ID string) (model.User, error) {
	mongoID, err := primitive.ObjectIDFromHex(ID)

	if err != nil {
		return nil, err
	}

	filter := bson.M{"_id": mongoID}
	result := &User{}

	if err := u.store.client.
		Database("chatserver").
		Collection("users").
		FindOne(context.Background(), filter).
		Decode(result); err != nil {
			if err == store.ErrNotFound {
				return nil, mongo.ErrNoDocuments
			}

			return nil, err
	}

	return result, nil
}

func (u UserRepository) GetAllUsers() ([]model.User, error) {
	var result []model.User

	cur, err := u.store.client.
		Database("chatserver").
		Collection("users").
		Find(context.Background(), bson.D{})

	if err != nil {
		return nil, err
	}

	for cur.Next(context.Background()) {
		var user *User

		if err := cur.Decode(&user); err != nil {
			return nil, err
		}

		result = append(result, user)
	}

	if err := cur.Err(); err != nil {
		return nil, err
	}

	if err := cur.Close(context.Background()); err != nil {
		return nil, err
	}

	return result, nil
}
