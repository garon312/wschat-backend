package mongostore_test

import (
	"chat-backend/internal/app/store/mongostore"
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestUserRepository_AddUser(t *testing.T) {
	store, truncate := mongostore.TestDB(t, databaseURL)
	defer truncate("users")

	u := mongostore.TestUser(t)

	assert.NoError(t, store.User().AddUser(u))
	assert.NotNil(t, u.GetID())
}

func TestUserRepository_GetAllUsers(t *testing.T) {
	usersCount := 10
	store, truncate := mongostore.TestDB(t, databaseURL)
	defer truncate("users")

	for i := 0; i < usersCount; i++ {
		u := &mongostore.User{
			Name: fmt.Sprintf("Test User #%d", i),
		}

		assert.NoError(t, store.User().AddUser(u))
		assert.NotNil(t, u.ID)
	}

	users, err := store.User().GetAllUsers()

	assert.NoError(t, err)
	assert.NotNil(t, users)
	assert.Len(t, users, usersCount)

	for i, u := range users {
		assert.Equal(t, u.GetName(), fmt.Sprintf("Test User #%d", i))
		assert.NotNil(t, u.GetID())
	}
}

func TestUserRepository_FindUserByID(t *testing.T) {
	store, truncate := mongostore.TestDB(t, databaseURL)
	defer truncate("users")

	testUser := mongostore.TestUser(t)

	assert.NoError(t, store.User().AddUser(testUser))
	assert.NotNil(t, testUser.GetID())

	user, err := store.User().FindUserByID(testUser.GetID())

	assert.NoError(t, err)
	assert.NotNil(t, user)
	assert.Equal(t, testUser.GetID(), user.GetID())
	assert.Equal(t, testUser.GetName(), user.GetName())
}

func TestUserRepository_RemoveUser(t *testing.T) {
	store, truncate := mongostore.TestDB(t, databaseURL)
	defer truncate("users")

	testUser := mongostore.TestUser(t)

	assert.NoError(t, store.User().AddUser(testUser))
	assert.NotNil(t, testUser.GetID())

	err := store.User().RemoveUser(testUser)

	assert.NoError(t, err)

	user, err := store.User().FindUserByID(testUser.GetID())

	assert.Error(t, err)
	assert.Nil(t, user)
}