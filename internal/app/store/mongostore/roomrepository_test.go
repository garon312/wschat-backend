package mongostore_test

import (
	"chat-backend/internal/app/store/mongostore"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestRoomRepository_AddRoom(t *testing.T) {
	store, truncate := mongostore.TestDB(t, databaseURL)
	defer truncate("rooms")

	r := mongostore.TestRoom(t)

	assert.NoError(t, store.Room().AddRoom(r))
	assert.NotNil(t, r)
	assert.NotNil(t, r.GetID())
}

func TestRoomRepository_FindRoomByName(t *testing.T) {
	store, truncate := mongostore.TestDB(t, databaseURL)
	defer truncate("rooms")

	testRoom := mongostore.TestRoom(t)

	assert.NoError(t, store.Room().AddRoom(testRoom))

	room, err := store.Room().FindRoomByName(testRoom.Name)

	assert.NoError(t, err)
	assert.NotNil(t, room)
	assert.NotNil(t, room.GetID())
	assert.Equal(t, testRoom.GetName(), room.GetName())
}
