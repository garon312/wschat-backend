package mongostore

import (
	"chat-backend/internal/app/store"
	"context"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type Store struct {
	client *mongo.Client
	roomRepository *RoomRepository
	userRepository *UserRepository
}

func New(dbURL string) (*Store, error) {
	client, err := mongo.NewClient(options.Client().ApplyURI(dbURL))

	if err != nil {
		return nil, err
	}

	st := &Store{
		client: client,
	}

	return st, nil
}

func (s *Store) Room() store.RoomRepository {
	if s.roomRepository != nil {
		return s.roomRepository
	}

	s.roomRepository = &RoomRepository{
		store: s,
	}

	return s.roomRepository
}

func (s *Store) User() store.UserRepository {
	if s.userRepository != nil {
		return s.userRepository
	}

	s.userRepository = &UserRepository{
		store: s,
	}

	return s.userRepository
}

func (s *Store) Connect() error {
	if err := s.client.Connect(context.Background()); err != nil {
		return err
	}

	if err := s.client.Ping(context.Background(), nil); err != nil {
		return err
	}

	return nil
}

func (s *Store) Disconnect() error {
	if err := s.client.Disconnect(context.Background()); err != nil {
		return err
	}

	return nil
}
