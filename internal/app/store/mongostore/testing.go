package mongostore

import (
	"chat-backend/internal/app/model"
	"context"
	"testing"
)

// TestDB sets up mongodb connection and truncates database
func TestDB(t *testing.T, databaseURL string) (*Store, func(...string)) {
	t.Helper()

	db, err := New(databaseURL)

	if err != nil {
		t.Fatal(err)
	}

	if err = db.Connect(); err != nil {
		t.Fatal(err)
	}

	return db, func(collections ...string) {
		if len(collections) > 0 {
			for _, c := range collections {
				if err := db.client.Database("chatserver").Collection(c).Drop(context.Background()); err != nil {
					t.Fatal(err)
				}

				if err := db.client.Database("chatserver").CreateCollection(context.Background(), c); err != nil {
					t.Fatal(err)
				}
			}
		}

		if err := db.Disconnect(); err != nil {
			t.Fatal(err)
		}
	}
}

// TestRoom provides basic mock Room
func TestRoom(t *testing.T) *Room {
	t.Helper()

	return &Room{
		Name:    "Test Room",
		Private: false,
	}
}

// TestUser provides basic mock User
func TestUser(t *testing.T) model.User {
	t.Helper()

	return &User{
		Name:    "Test Room",
	}
}

