package store

import "chat-backend/internal/app/model"

type RoomRepository interface {
	AddRoom(model.Room) error
	FindRoomByName(string) (model.Room, error)
}

type UserRepository interface {
	AddUser(model.User) error
	RemoveUser(model.User) error
	FindUserByID(string) (model.User, error)
	GetAllUsers() ([]model.User, error)
}
