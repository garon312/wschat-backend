package model

// Room represents room data model
type Room interface {
	GetID() string
	SetID(string)
	GetName() string
	IsPrivate() bool
}
