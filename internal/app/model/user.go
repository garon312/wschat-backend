package model

type User interface {
	GetID() string
	GetName() string
	SetID(string)
}
