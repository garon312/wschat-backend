module chat-backend

go 1.15

require (
	github.com/BurntSushi/toml v0.3.1
	github.com/go-redis/redis/v8 v8.3.3
	github.com/google/uuid v1.1.2
	github.com/gorilla/mux v1.8.0
	github.com/gorilla/websocket v1.4.2
	github.com/sirupsen/logrus v1.7.0
	github.com/stretchr/testify v1.6.1
	go.mongodb.org/mongo-driver v1.4.2
)
