package main

import (
	"chat-backend/internal/app/httpserver"
	"flag"
	"log"

	"github.com/BurntSushi/toml"
)

var configPath string

func init() {
	flag.StringVar(&configPath, "c", "configs/server.toml", "path to config file")
}

func main() {
	flag.Parse()

	config := httpserver.NewConfig()

	_, err := toml.DecodeFile(configPath, config)

	if err != nil {
		log.Fatal(err)
	}

	if err := httpserver.Start(config); err != nil {
		log.Fatal(err)
	}
}
